const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));

require("dotenv").config();
const Discord = require("discord.js");
const client = new Discord.Client({ intents: [Discord.Intents.FLAGS.GUILDS, Discord.Intents.FLAGS.GUILD_MESSAGES] });
const { red } = require("chalk");

client.commands = new Discord.Collection();
client.aliases = new Discord.Collection();
client.categories = new Discord.Collection();
client.events = new Discord.Collection();

client.guessEmojiStreaks = new Discord.Collection();

["commandHandler", "eventHandler"].forEach((handler) => {
	require(`./handlers/${handler}`)(client, Discord)
});

process.on("uncaughtException", (err) => {
	console.log(red("[rejection]", err));
});

client.login(process.env.TOKEN);