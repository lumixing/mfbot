const { green } = require("chalk");

module.exports = (Discord, client, msg) => {
	console.log(green(`${client.user.tag} has logged in`));
}