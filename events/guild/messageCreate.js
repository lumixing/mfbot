const { prefix, developers } = require("../../config.json");

module.exports = (Discord, client, msg) => {
	if (!msg.content.startsWith(prefix)) return;
	if (msg.author.bot) return;

	let args = msg.content.slice(prefix.length).split(/ +/);
	const cmd = args.shift().toLowerCase();
	let flag;

	if (args.length && args[args.length - 1].match(/-\w/g)) {
		console.log(`${args[args.length - 1]} matches as a flag`);
		flag = args[args.length - 1];
		args.pop();
	}

	const command = client.commands.get(cmd) || client.aliases.get(cmd);

	if (!command) return;

	if (command.permissions.length) {
		let missingPerms = [];

		for (const perm of command.permissions) {
			if (perm === "DEVELOPER" && !developers.includes(msg.author.id)) {
				missingPerms.push(perm);
				break;
			}

			if (!msg.member.permissions.has(perm)) {
				missingPerms.push(perm);
			}
		};

		if (missingPerms.length) {
			msg.reply({ content: `you are missing permissions to run that command: \`${missingPerms.join(", ")}\``, failIfNotExists: false });
			return;
		}
	}

	command.execute(client, msg, args, flag, Discord);
}