const { readdirSync } = require("fs");

module.exports = (client, Discord) => {
	const categoryFolders = readdirSync("./commands");

	for (const folder of categoryFolders) {
		const commandFiles = readdirSync(`./commands/${folder}`).filter((file) => file.endsWith(".js"));

		for (const file of commandFiles) {
			const command = require(`../commands/${folder}/${file}`);
			client.commands.set(command.name, command);

			// alias handler
			if (command.aliases.length) {
				command.aliases.forEach((alias) => {
					client.aliases.set(alias, command);
				});
			}

			// category handler
			if (!client.categories.has(folder)) {
				client.categories.set(folder, new Discord.Collection().set(command.name, command));
			}
			else {
				client.categories.get(folder).set(command.name, command);
			}

			command.category = folder;
		}
	}

}