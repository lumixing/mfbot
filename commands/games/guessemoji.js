const { emojis } = require("../../assets/emojis.json");
const { GREEN, RED } = require("../../assets/colors.json");

module.exports = {
	async execute(client, msg, args, flag, Discord) {
		const streaks = client.guessEmojiStreaks;
		const rng = Math.floor(Math.random() * emojis.length);

		const emojiMessage = await msg.reply({ content: `:${emojis[rng]}:`, failIfNotExists: false });

		const filter = (m) => m.author.id === msg.author.id;
		msg.channel.awaitMessages({ filter, time: 60000, max: 1, errors: ["time"] })
			.then((m) => {
				const response = m.first().content.toLowerCase().replaceAll(" ", "_");

				if (!streaks.has(msg.guild.id)) {
					streaks.set(msg.guild.id, 0);
				}

				let currentStreak = streaks.get(msg.guild.id);

				if (response === emojis[rng]) {
					streaks.set(msg.guild.id, currentStreak + 1);
					currentStreak = streaks.get(msg.guild.id)

					emojiMessage.reply({
						content: `<@!${msg.author.id}>`,
						embeds: [{
							color: GREEN,
							description: `**${msg.author.username}:** ${response}\n**emojiAI:** ${emojis[rng]}\nPOG streak is now \`${currentStreak}\``,
						}]
					});
				}
				else {
					streaks.set(msg.guild.id, 0);

					emojiMessage.reply({
						content: `<@!${msg.author.id}>`,
						embeds: [{
							color: RED,
							description: `**${msg.author.username}:** ${response}\n**emojiAI:** ${emojis[rng]}\nu ruined the \`${currentStreak}\` streak`,
						}]
					});
				}
			})
			.catch((err) => {
				streaks.set(msg.guild.id, 0);

				emojiMessage.reply({
					content: `<@!${msg.author.id}>`,
					embeds: [{
						color: RED,
						description: `**${msg.author.username}:** ran out of time!\n**emojiAI:** ${emojis[rng]}\nu ruined the \`${currentStreak}\` streak`,
					}]
				});
			});
	},
	name: "guessemoji",
	aliases: ["ge"],
	description: "guess the name of the given emoji",
	usage: "guessemoji",
	permissions: [],
	flags: []
}