const { prefix } = require("../../config.json");

module.exports = {
	async execute(client, msg, args, flag, Discord) {

		if (!args.length) {
			msg.reply({ content: "you need to enter some code", failIfNotExists: false });
			return;
		}

		const code = args.join(" ");

		try {
			let result = await eval(code);
			result = JSON.stringify(result, null, 2);
			result = Discord.Formatters.codeBlock("json", result);

			msg.reply({ content: result, failIfNotExists: false })
				.catch((err) => {
					if (String(err).match(/Must be 2000 or fewer in length/g)) {
						msg.reply({ content: `message is more than 2000 characters, try \`${prefix}log\` instead`, failIfNotExists: false });
					}
				});
		}
		catch (err) {
			msg.reply({ content: err.toString(), failIfNotExists: false });
			return;
		}

	},
	name: "eval",
	aliases: ["ev"],
	description: "evaluates js code",
	usage: "eval [code]",
	permissions: ["DEVELOPER"],
	flags: []
}