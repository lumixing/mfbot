const { prefix } = require("../../config.json");

module.exports = {
	async execute(client, msg, args, flag, Discord) {

		if (!args.length) {
			msg.reply({ content: "you need to enter some code", failIfNotExists: false });
			return;
		}

		const code = args.join(" ");

		try {
			let result = await eval(code);
			console.log(result);
			msg.react("✅").catch((err) => 0);
		}
		catch (err) {
			msg.reply({ content: err.toString(), failIfNotExists: false });
			return;
		}

	},
	name: "log",
	aliases: [],
	description: "logs js code",
	usage: "log [code]",
	permissions: ["DEVELOPER"],
	flags: []
}