const ms = require("humanize-duration");

module.exports = {
	async execute(client, msg, args, flag, Discord) {
		let user = msg.author;
		let member = msg.member;
		let mention = msg.mentions.members.first();

		if (args.length && !mention) {
			msg.reply({ content: "you need to mention a user", failIfNotExists: false });
			return;
		}

		if (args.length && mention.user) {
			user = mention.user;
			member = mention;
		}

		let currentDate = new Date();
		let createdDate = new Date(user.createdTimestamp);
		let joinedDate = new Date(member.joinedTimestamp);
		let options = { largest: 2, round: true };

		let descriptionArray = [
			"**__account creation__**",
			`**date**: ${createdDate.toLocaleString()}`,
			`**age**: ${ms(currentDate - createdDate, options)}`,

			"\n**__server joined__**",
			`**date**: ${joinedDate.toLocaleString()}`,
			`**age**: ${ms(currentDate - joinedDate, options)}`,
		];

		msg.reply({
			embeds: [{
				color: 0xffffff,
				author: {
					name: user.tag,
					icon_url: user.avatarURL()
				},
				description: descriptionArray.join("\n"),
				footer: {
					text: `ms: ${createdDate.getTime()} | ${joinedDate.getTime()}`
				}
			}], failIfNotExists: false
		});
	},
	name: "age",
	aliases: [],
	description: "shows the account age of a user",
	usage: "age (@mention)",
	permissions: [],
	flags: []
}