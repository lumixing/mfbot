module.exports = {
	async execute(client, msg, args, flag, Discord) {
		msg.reply({ content: "https://lumixing.github.io/mfbot/", failIfNotExists: false });
	},
	name: "website",
	aliases: ["web", "site"],
	description: "sends the mfbot website link",
	usage: "website",
	permissions: [],
	flags: []
}