const { prefix } = require("../../config.json");

module.exports = {
	async execute(client, msg, args, flag, Discord) {
		if (!args.length) {
			let descriptionArray = [];

			client.categories.each((key, value) => {
				descriptionArray.push(`\`${value}\` (${key.size} commands)`);
			});

			msg.reply({
				embeds: [{
					color: 0xffffff,
					title: "mfbot command categories",
					title: `type \`${prefix}help [category name]\` for a list of commands`,
					description: descriptionArray.join("\n"),
					footer: {
						text: `${client.commands.size} total commands`
					}
				}], failIfNotExists: false
			});

			return;
		}

		let input = args[0].toLowerCase();

		if (!client.commands.has(input) && !client.aliases.has(input) && !client.categories.has(input)) {
			msg.reply({ content: `that command or category name does not exist\ntype \`${prefix}help\` for more help`, failIfNotExists: false });
			return;
		}

		if (client.categories.has(input)) {
			let commandsArray = [];

			client.categories.get(input).each((key) => {
				commandsArray.push(`\`${prefix}${key.name}\``);
			});

			msg.channel.send({
				embeds: [{
					color: 0xffffff,
					title: `list of commands in ${input}\ntype \`${prefix}help [command name]\` for more info about a command`,
					description: commandsArray.join(" "),
					footer: {
						text: `${commandsArray.length} commands`
					}
				}], failIfNotExists: false
			});

			return;
		}

		let command = client.commands.get(input) || client.aliases.get(input);

		let descriptionArray = [
			`**name**: ${command.name}`,
			`**aliases**: ${command.aliases.length ? command.aliases.join(", ") : "-"}`,
			`**description**: ${command.description}`,
			`**usage**: ${command.usage}`,
			`**category**: ${command.category}`,
			`**permissions**: ${command.permissions.length ? command.permissions.join(", ") : "-"}`,
		];

		if (command.flags.length) {
			command.flags.forEach((flag) => {
				descriptionArray.push(`**flag ${flag[0]}**: ${flag[1]}`);
			});
		}

		msg.reply({
			embeds: [{
				color: 0xffffff,
				title: prefix + command.name,
				description: descriptionArray.join("\n")
			}], failIfNotExists: false
		});
	},
	name: "help",
	aliases: ["h"],
	description: "gives info about a command or category",
	usage: "help (command or category name)",
	permissions: [],
	flags: []
}