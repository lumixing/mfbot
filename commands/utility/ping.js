module.exports = {
	async execute(client, msg, args, flag, Discord) {
		msg.reply({ content: `${msg.client.ws.ping}ms`, failIfNotExists: false });
	},
	name: "ping",
	aliases: ["p"],
	description: "shows ping of mfbot",
	usage: "ping",
	permissions: [],
	flags: []
}