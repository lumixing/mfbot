module.exports = {
	async execute(client, msg, args, flag, Discord) {
		msg.reply({ content: "https://twitter.com/mfbot_", failIfNotExists: false });
	},
	name: "twitter",
	aliases: ["tw"],
	description: "sends the mfbot twitter link",
	usage: "twitter",
	permissions: [],
	flags: []
}