module.exports = {
	async execute(client, msg, args, flag, Discord) {
		msg.reply({ content: await client.generateInvite({ scopes: ["bot"] }), failIfNotExists: false });
	},
	name: "invite",
	aliases: ["inv"],
	description: "sends the bot invitation link",
	usage: "invite",
	permissions: [],
	flags: []
}