const { prefix } = require("../../config.json");

module.exports = {
	async execute(client, msg, args, flag, Discord) {
		let commandsArray = [];

		msg.client.commands.each((cmd) => {
			commandsArray.push(`\`${cmd.name}\``);
		});

		msg.reply({
			embeds: [{
				color: 0xffffff,
				title: "mfbot commands",
				title: `type \`${prefix}help [command name]\` for more info about a command`,
				description: commandsArray.join(" "),
				footer: {
					text: `${client.commands.size} commands in ${client.categories.size} categories`
				}
			}], failIfNotExists: false
		});
	},
	name: "commands",
	aliases: ["cmd", "cmds"],
	description: "shows a list of commands",
	usage: "commands",
	permissions: [],
	flags: []
}