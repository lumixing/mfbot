module.exports = {
	async execute(client, msg, args, flag, Discord) {
		msg.reply({ content: "https://github.com/lumixing/mfbot/", failIfNotExists: false });
	},
	name: "github",
	aliases: ["git", "gh", "repo"],
	description: "sends the github repo link",
	usage: "github",
	permissions: [],
	flags: []
}