module.exports = {
	async execute(client, msg, args, Discord) {
		let member = msg.mentions.members.first();

		if (!member) {
			msg.reply({ content: "you need to mention a member", failIfNotExists: false });
			return;
		}

		args.shift();

		if (!args.length) {
			msg.reply({ content: "you need to enter a message to send", failIfNotExists: false });
			return;
		}

		member.send(args.join(" "))
			.then(() => msg.reply({ content: `sent message to ${member.user.tag}`, failIfNotExists: false }))
			.catch((err) => msg.reply({ content: `could not send message to user, maybe they disabled dms for me, this guild or globally`, failIfNotExists: false }));
	},
	name: "tell",
	aliases: [],
	description: "sends a dm to a user",
	usage: "tell [message]",
	permissions: [],
	flags: []
}