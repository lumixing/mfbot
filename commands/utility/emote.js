module.exports = {
	async execute(client, msg, args, Discord) {
		if (!args.length || !args[0].match(/<a:.+?:\d+>|<:.+?:\d+>/g)) {
			msg.reply({ content: "you need to enter a valid emote", failIfNotExists: false });
			return;
		}

		// could use .resolveIdentifier but its bugged
		const emote = msg.guild.emojis.cache.find((e) => `<${args[0][1] === "a" ? "" : ":"}${e.identifier}>` === args[0]);

		if (!emote) {
			msg.reply({ content: "that emote is not from this server", failIfNotExists: false });
			return;
		}

		let descriptionArray = [
			`**name**: ${emote.name}`,
			`**id**: ${emote.id}`,
			`**created at**: ${new Date(emote.createdTimestamp).toLocaleString()}`,
			`**uploaded by**: ${(await emote.fetchAuthor()).tag}`
		];

		msg.reply({
			embeds: [{
				color: 0xffffff,
				description: descriptionArray.join("\n"),
				thumbnail: {
					url: emote.url
				}
			}], failIfNotExists: false
		});
	},
	name: "emote",
	aliases: ["emoji", "em"],
	description: "gets info about server emoji",
	usage: "emote [:emote:]",
	permissions: [],
	flags: []
}