const { dependencies, version } = require("../../package.json");

function msToHHMMSS(ms) {
	let sec_num = parseInt(Math.floor(ms / 1000), 10);
	let hours = Math.floor(sec_num / 3600);
	let minutes = Math.floor(sec_num / 60) % 60;
	let seconds = sec_num % 60;

	return [hours, minutes, seconds]
		.map(v => v < 10 ? "0" + v : v)
		.filter((v, i) => v !== "00" || i > 0)
		.join(":");
}

module.exports = {
	async execute(client, msg, args, flag, Discord) {
		let descriptionArray = [
			`**version**: ${version}`,
			`**discord.js version**: ${dependencies["discord.js"]}`,
			`**latency**: ${msg.client.ws.ping}ms`,
			`**uptime**: ${msToHHMMSS(msg.client.uptime)}`,
			`**ready at**: ${msg.client.readyAt.toLocaleString()}`
		];

		msg.reply({
			embeds: [{
				color: 0xffffff,
				title: "about mfbot",
				description: descriptionArray.join("\n")
			}], failIfNotExists: false
		});
	},
	name: "about",
	aliases: ["a"],
	description: "info about mfbot",
	usage: "about",
	permissions: [],
	flags: []
}