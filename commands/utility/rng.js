module.exports = {
	async execute(client, msg, args, flag, Discord) {
		if (!args.length) {
			if (flag === "-b") {
				msg.reply({ content: Math.random() < 0.5 ? "true" : "false", failIfNotExists: false });
				return;
			}

			msg.reply({ content: Math.random().toString(), failIfNotExists: false });
			return;
		}

		const min = parseInt(args[0]);
		const max = parseInt(args[1]);

		if (!args[1]) {
			msg.reply({ content: "you need to provide a max number aswell", failIfNotExists: false });
			return;
		}

		if (isNaN(min) || isNaN(max)) {
			msg.reply({ content: "min and max need to be integer numbers", failIfNotExists: false });
			return;
		}

		// random integer function
		const randomInteger = Math.floor(Math.random() * (max - min + 1)) + min;

		msg.reply({ content: randomInteger.toString(), failIfNotExists: false })
			.catch((err) => msg.reply({ content: `could not send message\n\`${err}\``, failIfNotExists: false }));
	},
	name: "rng",
	aliases: ["random", "rand"],
	description: "generates a random number",
	usage: "rng (min) (max)",
	permissions: [],
	flags: [
		["-b", "random boolean"]
	]
}