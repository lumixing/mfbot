const { evaluate } = require("mathjs");

module.exports = {
	async execute(client, msg, args, flag, Discord) {
		if (!args.length) {
			msg.reply({ content: "you need to enter an equation", failIfNotExists: false });
			return;
		}

		try {
			const result = evaluate(args.join(" "));
			msg.reply({ content: result.toString(), failIfNotExists: false });
		}
		catch (err) {
			msg.reply({ content: `an error occured: \`${err}\``, failIfNotExists: false });
		}
	},
	name: "calc",
	aliases: ["calculate"],
	description: "calculate an equation",
	usage: "calc [equation]",
	permissions: [],
	flags: []
}