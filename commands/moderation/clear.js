module.exports = {
	async execute(client, msg, args, flag, Discord) {
		const messageCount = parseInt(args[0]) || 100;

		if (messageCount < 1 || messageCount > 100) {
			msg.reply({ content: "amount of messages can only be a number between 1 and 100", failIfNotExists: false });
			return;
		}

		msg.channel.bulkDelete(messageCount, true)
			.then((m) => msg.reply({ content: `deleted ${m.size} message${m.size === 1 ? "" : "s"}`, failIfNotExists: false }));
	},
	name: "clear",
	aliases: ["c"],
	description: "clears messages from channel",
	usage: "clear [amount of messages]",
	permissions: ["MANAGE_MESSAGES"],
	flags: []
}