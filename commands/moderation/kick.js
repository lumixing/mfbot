module.exports = {
	async execute(client, msg, args, flag, Discord) {
		const member = msg.mentions.members.first();

		if (!args.length || !member) {
			msg.reply({ content: "you need to mention a member to kick", failIfNotExists: false });
			return;
		}

		if (member.equals(msg.member)) {
			msg.reply({ content: "you really want to kick yourself?", failIfNotExists: false });
			return;
		}

		if (!member.kickable) {
			msg.reply({ content: "that member cannot be kicked", failIfNotExists: false });
			return;
		}

		args.shift();
		const reason = args.join(" ");

		try { await member.send(`you have been kicked from ${msg.guild.name} by ${msg.author.tag} ${reason.length ? `for ${reason}` : ""}`) }
		catch (err) { 0 } // https://github.com/discordjs/discord.js/issues/4112#:~:text=The%20bot%20and,bot%20(including%20itself)

		member.kick(reason.length ? reason : null);
		msg.reply({ content: `${member.user.tag} has been kicked from the server`, failIfNotExists: false });
	},
	name: "kick",
	aliases: [],
	description: "kick someone off the server",
	usage: "kick [@member] (reason)",
	permissions: ["KICK_MEMBERS"],
	flags: []
}