module.exports = {
	async execute(client, msg, args, flag, Discord) {
		const member = msg.mentions.members.first();

		if (!args.length || !member) {
			msg.reply({ content: "you need to mention a member to ban", failIfNotExists: false });
			return;
		}

		if (member.equals(msg.member)) {
			msg.reply({ content: "you really want to ban yourself?", failIfNotExists: false });
			return;
		}

		if (!member.bannable) {
			msg.reply({ content: "that member cannot be banned", failIfNotExists: false });
			return;
		}

		args.shift();
		const reason = args.join(" ");

		try { await member.send(`you have been banned from ${msg.guild.name} by ${msg.author.tag} ${reason.length ? `for ${reason}` : ""}`) }
		catch (err) { 0 } // https://github.com/discordjs/discord.js/issues/4112#:~:text=The%20bot%20and,bot%20(including%20itself)

		member.ban({ reason: reason.length ? reason : null });
		msg.reply({ content: `${member.user.tag} has been banned from the server`, failIfNotExists: false });
	},
	name: "ban",
	aliases: [],
	description: "ban someone from the server",
	usage: "ban [@member] (reason)",
	permissions: ["BAN_MEMBERS"],
	flags: []
}